Name:          kiran-authentication-service
Version:       2.5.1
Release:       9
Summary:       Kiran Desktop kiran authentication service
License:       MulanPSL-2.0
URL:           http://www.kylinsec.com.cn

Source0:       %{name}-%{version}.tar.gz

Patch0001:	0001-fix-auth-terminal-Repair-authentication-type-check.patch
Patch0002:	0002-fix-pam-conf-Adjust-the-number-of-non-password-authe.patch
Patch0003:	0003-fix-Interface-permission-Upgrade-the-permission-of-s.patch
Patch0004:	0004-fix-multi-factor-Fixed-an-authentication-failure-cau.patch
Patch0005:	0005-fix-auth-order-Adjust-the-authentication-sequence.patch
Patch0006:	0006-fix-default-device-Device-adapters-do-not-update-def.patch
Patch0007:	0007-fix-multi-factor-Multifactor-authentication-handling.patch
Patch0008:	0008-fix-default-device-Update-the-logic-of-the-default-a.patch
Patch0009:	0009-fix-multi-factor-multi-factor-no-jump-login.patch
Patch0010:	0010-fix-multi-channel-auth-If-the-authentication-fails-t.patch
Patch0011:	0011-feat-auth-error-Subdivide-the-cause-of-the-error-and.patch
Patch0012:	0012-fix-kiran-authentication-service-fix-for-versions-ea.patch

Obsoletes:  kiran-biometrics

BuildRequires: systemd
BuildRequires: systemd-devel
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: cmake
BuildRequires: make
BuildRequires: pam-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-linguist
BuildRequires: kiran-log-qt5-devel
BuildRequires: kiran-cc-daemon-devel
BuildRequires: kiran-authentication-devices-devel

Requires: pam
Requires: qt5-qtbase
Requires: kiran-log-qt5
Requires: kiran-system-daemon
Requires: kiran-session-daemon
Requires: kiran-authentication-devices
%{?systemd_requires}

%description
Kiran authentication service is used to do system auth with password, fingerprint, face

%package devel
Summary:      Development files for kiran authentication service
Requires:     %{name} = %{version}-%{release}

%description devel
Development files for kiran authentication service 

%prep
%autosetup -p1

%build
%cmake
%cmake_build

%install
%cmake_install

%post -n kiran-authentication-service
%systemd_post kiran-authentication-daemon.service
systemctl enable kiran-authentication-daemon.service

%preun -n kiran-authentication-service
%systemd_preun kiran-authentication-daemon.service

%files
%{_datadir}/polkit-1/actions/com.kylinsec.Kiran.Authentication.policy
%{_sysconfdir}/dbus-1/system.d/com.kylinsec.Kiran.Authentication.conf
%{_datadir}/dbus-1/system-services/com.kylinsec.Kiran.Authentication.service
%{_prefix}/lib/systemd/system/kiran-authentication-daemon.service
%{_sysconfdir}/kiran-authentication-service/kad.ini
%{_sysconfdir}/pam.d/kiran-authentication-service
%{_datadir}/kiran-authentication-service/translations/kiran-authentication-daemon.zh_CN.qm
%{_datadir}/kiran-authentication-service/translations/kiran-authentication-daemon.bo_CN.qm
%{_datadir}/kiran-authentication-service/translations/kiran-authentication-daemon.kk_KZ.qm
%{_datadir}/kiran-authentication-service/translations/kiran-authentication-daemon.ky_KG.qm
%{_datadir}/kiran-authentication-service/translations/kiran-authentication-daemon.mn_MN.qm
%{_datadir}/kiran-authentication-service/translations/kiran-authentication-daemon.ug_CN.qm
%{_bindir}/kiran-authentication-daemon
%{_datadir}/kiran-authentication-service/translations/pam_kiran_authentication.zh_CN.qm
%{_datadir}/kiran-authentication-service/translations/pam_kiran_authentication.bo_CN.qm
%{_datadir}/kiran-authentication-service/translations/pam_kiran_authentication.kk_KZ.qm
%{_datadir}/kiran-authentication-service/translations/pam_kiran_authentication.ky_KG.qm
%{_datadir}/kiran-authentication-service/translations/pam_kiran_authentication.mn_MN.qm
%{_datadir}/kiran-authentication-service/translations/pam_kiran_authentication.ug_CN.qm
%{_libdir}/security/pam_kiran_authentication.so

%files devel
%{_includedir}/kiran-authentication-service/kas-authentication-i.h

%changelog
* Mon Dec 16 2024 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.1-9
- add obsoletes kiran-biometrics

* Wed Nov 20 2024 Funda Wang <fundawang@yeah.net> - 2.5.1-8
- adopt to new cmake macro

* Mon Sep 11 2023 yinhongchang <yinhongchang@kylinsec.com.cn> - 2.5.1-7
- KYOS-F: fix for versions earlier than qt5.10 in kiranUI-2.5(#15019)

* Thu Jun 15 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.1-6
- KYOS-B: Subdivide the cause of the error and determine whether to record the error according to the cause and mode(#I7DCKL)

* Sat Jun 03 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.1-5
- KYOS-B:  If the authentication fails, the faillock module counts the data(#I7937W)

* Fri Jun 02 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.1-4
- KYOS-B: Device adapters do not update default devices that do not exist
- KYOS-B: Multifactor authentication, handling only password authentication
- KYOS-B: Update the logic of the default authentication device
- KYOS-B: multi-factor no jump login

* Wed May 31 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.1-3
- KYOS-B: fix terminl authentication type check (#I792B4)
- KYOS-B: Adjust the number of non-password authentication failures recorded in the PAM configuration file(#I7937W)
- KYOS-B: Upgrade the permission of some interfaces of the authentication service(#I795QI)
- KYOS-B: Fixed an authentication failure caused by disabling all authentication modes during multi-factor authentication(#I79I33)
- KYOS-B: Adjust the authentication sequence(#I79FVY)

* Wed May 24 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.1-2
- KYOS-F: Remove Require kiran-control-panel

* Wed May 24 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.1-1
- KYOS-F: Update translation file and modify the pam configuration file
- KYOS-F: Adjust enumeration values to accommodate some of the prompt messages during entry
- KYOS-F: Supports UKey, Iris,face authentication, support mulit-factory auth,update error count recording mode
- KYOS-F: Add the Ukey authentication,DeviceAdaptor occupy device timer , Number of user binding feature

* Mon Apr 24 2023 liuxinaho <liuxinhao@kylinsec.com.cn> - 2.5.0-7
- KYOS-F: fix the DBus Service Exec field problem 

* Sat Apr 22 2023 kpkg <kpkg.kylinsec.com.cn> - 2.5.0-6
- rebuild for KY3.4-5-GC-KiranUI-2.5

* Sat Apr 22 2023 kpkg <kpkg.kylinsec.com.cn> - 2.5.0-5
- rebuild for KiranUI-2.5-next

* Sat Apr 22 2023 wangyucheng <wangyucheng@kylinsec.om.cn> - 2.5.0-4
- KYOS-F: fix compile error: no match for ‘operator<<’ on gcc 7.3.0.

* Fri Apr 21 2023 wangyucheng <wangyucheng@kylinsec.om.cn> - 2.5.0-3
- KYOS-F: qt5 cmake command compatible. 

* Sat Apr 08 2023 wangyucheng <wangyucheng@kylinsec.om.cn> - 2.5.0-2
- KYOS-T: add some translation

* Tue Apr 04 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.5.0-1
- KYOS-F: New version authentication service

* Wed Mar 01 2023 wangxiaoqing <wangxiaoqing@kylinsec.com.cn> - 0.0.2-2
- KYOS-B: Modify the log information level of zlog initialization failure to debug.

* Thu Oct 27 2022 wangxiaoqing <wangxiaoqing@kylinsec.com.cn> - 0.0.2-1
- KYOS-B: Do not exit when zlog init failed.

* Wed Aug 10 2022 luoqing <luoqing@kylinsec.com.cn> - 0.0.1-2.kb4
- KYOS-F: Modify license and add yaml file.

* Tue Jan 25 2022 wxq <wangxiaoqing@kylinos.com.cn> - 0.0.1-2.kb3
- KYOS-B:  Add the enable zlog ex macro for zlog.

* Mon Jan 24 2022 wxq <wangxiaoqing@kylinos.com.cn> - 0.0.1-2.kb2
- KYOS-F: Add the requires for develop package.

* Wed Dec 29 2021 kpkg <kpkg@kylinos.com.cn> - 0.0.1-2.kb1
- rebuild for KY3.4-MATE-modules-dev

* Wed Dec 29 2021 caoyuanji<caoyuanji@kylinos.com.cn> - 0.0.1-2
- Upgrade version number for easy upgrade

* Fri Dec 24 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 0.0.1-1.kb6
- rebuild for KY3.4-4-KiranUI-2.2

* Thu Dec 23 2021 wxq <wangxiaoqing@kylinos.com.cn> - 0.0.1.kb5
- KYOS-B: Add the requires for develop package.

* Tue Aug 24 2021 wxq <wangxiaoqing@kylinos.com.cn> - 0.0.1
- KYOS-F: build 0.0.1 version. (#38334)
